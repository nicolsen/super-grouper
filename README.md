# Super Grouper

_Author_: Nic Olsen

The `groups.py` script takes an input file describing students by their name, proficiency level in each subject, and a list of students that they should not be grouped with. The output of the script is a set of groups of roughly the requested size which respect the grouping restrictions. These groups also respect a given tolerance for differences in proficiency of the given subject.

Depending on the students, desired group size, and tolerance, it may be impossible to produce even groups. In this case, the user may choose to move a few students between groups.


## Usage

```
grouper [-h] [--subject SUBJECT] [--tolerance TOLERANCE] student_file students_per_group
```