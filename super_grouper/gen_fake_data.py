"""
(c) 2022 Nic Olsen
See LICENSE for full details
"""

import numpy as np
import json
import string


def gen_fake_data(n: int, frac_excludes: float = 0.25, name_gen = string.ascii_letters):
    math_prof = np.random.randint(1, 4, n)
    reading_prof = np.random.randint(1, 4, n)
    math_prof = np.random.randint(1, 4, n)

    # Weird excludes generation
    excludes = [name_gen[i] for i in np.random.randint(0, n, n)]
    for i in np.random.randint(0, n, int((1 - frac_excludes) * n)):
        excludes[i] = None

    d = [{"name": n, "math": int(m), "reading": int(r), "excludes": [e] if e is not None else []} for
         n, m, r, e in zip(name_gen[:n], math_prof, reading_prof, excludes)]

    with open("fake_students.json", "w") as outfile:
        json.dump(d, outfile)

def main():
    gen_fake_data(30, frac_excludes=0.1)

if __name__ == "__main__":
    main()