"""
(c) 2022 Nic Olsen
See LICENSE for full details
"""

import json
from pyvis.network import Network
import networkx as nx
import argparse
import numpy as np
import itertools


def sub_group(students, n_per_group, group_label, tolerance=None, subject=None):
    graph = nx.Graph()
    graph.add_nodes_from([s["name"] for s in students])
    graph.add_edges_from(itertools.combinations([s["name"] for s in students], 2))

    if tolerance is not None:
        for s_A, s_B in itertools.combinations(students, 2):
            diff = abs(s_A[subject] - s_B[subject]) 
            if diff > tolerance:
                graph.remove_edge(s_A["name"], s_B["name"])

    # Remove excludes
    for student in students:
        for exclude in student["excludes"]:
            pair = (student["name"], exclude)
            if pair in graph.edges:
                graph.remove_edge(*pair)


    comp = nx.complement(graph)

    # Save the graph
    net = Network(notebook=True)
    net.from_nx(graph)
    net.show(f"net_{group_label}.html")

    # Save the complement
    net = Network(notebook=True)
    net.from_nx(comp)
    net.show(f"comp_{group_label}.html")

    greedy = False
    try:
        # Only possible when n_groups > deg(comp)
        n_groups = len(students) // n_per_group
        coloring = nx.coloring.equitable_color(comp, n_groups)

    except nx.exception.NetworkXAlgorithmError:
        try:
            print("Falling back on greedy coloring")
            greedy = True
            coloring = nx.coloring.greedy_color(comp)

        except nx.exception.NetworkXAlgorithmError:
            print(f"Failed grouping {group_label}")
            return []

    groups = []
    n_colors = len(set([v for v in coloring.values()]))
    for c in range(n_colors):
        group = [s["name"] for s in students if coloring[s["name"]] == c]
        if group:
            groups.append(group)

    if greedy:
        for group in groups:
            split_into = len(group) // n_per_group
            if split_into > 1:
                for i in range(1, split_into):
                    sub_group = group[i * n_per_group : (i + 1) * n_per_group]
                    groups.append(sub_group)

                del group[n_per_group:]

    return groups


def group(subject: str, tolerance: int, n_per_group: int, students):
    students = np.random.permutation(students)
    profs = np.unique([s[subject] for s in students])
    if tolerance is not None and tolerance != 0:
        groups = sub_group(students, n_per_group, "all", tolerance=tolerance, subject=subject)
    else:
        groups = []
        for prof in profs:
            students_prof = [s for s in students if s[subject] == prof]
            n_groups_prof = int(np.ceil(len(students_prof) / n_per_group))

            print(f"Proficiency {prof}: {[s['name'] for s in students_prof]}")
            print(f"\tForming {n_groups_prof} groups...")

            sub_groups = sub_group(students_prof, n_groups_prof, f"{prof}")
            for group in sub_groups:
                groups.append(group)

    for i, group in enumerate(groups):
        print(f"Group {i}: {group}")


def main():
    parse = argparse.ArgumentParser()
    parse.add_argument("student_file")
    parse.add_argument("students_per_group", type=int)
    parse.add_argument("--subject")
    parse.add_argument("--tolerance", type=int, default=None)
    args = parse.parse_args()

    with open(args.student_file, "r") as infile:
        students = np.array(json.load(infile))

    subject = args.subject
    tolerance = args.tolerance
    n_per_group = args.students_per_group

    group(subject, tolerance, n_per_group, students)


if __name__ == "__main__":
    main()