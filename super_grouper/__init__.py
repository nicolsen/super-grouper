"""
(c) 2022 Nic Olsen
See LICENSE for full details
"""

__version__ = '0.1.0'
from .groups import group